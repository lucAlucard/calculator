package br.dreams.calculator.calculator.service;

import org.springframework.stereotype.Service;

@Service
public class CalculatorService {

    public Integer sum(Integer value1, Integer value2) {
        return value1 + value2;
    }    
}
