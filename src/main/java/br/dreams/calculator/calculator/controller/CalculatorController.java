package br.dreams.calculator.calculator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.dreams.calculator.calculator.service.CalculatorService;

@RestController
@RequestMapping(value = "calculate")
public class CalculatorController {

    @Autowired
    private CalculatorService calculatorService;
    
    @RequestMapping(value = "sum")
    public Integer sum(final @RequestParam("value1") Integer value1, final @RequestParam("value2") Integer value2) {
        return calculatorService.sum(value1, value2);
    }
    

}
